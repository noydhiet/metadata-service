const wrapper = require('../../utils/wrapper');
const getConnection = require('./connection');

const insertOne = async (payload) => {
  let connections = await getConnection.pool.acquire();
  let result = new Promise((resolve, reject) => {
    connections.index({
      index: payload.index,
      id: payload.id,
      type: payload.type,
      body: payload.body
    }, async (error, response, status) => {
      getConnection.pool.release(connections);
      if (error) {
        reject(error);
      }
      resolve({
        response: response,
        status: status
      });
    });
  });
  return await Promise.resolve(result)
    .then(ress => wrapper.data(ress.response, 'success insert data', ress.status))
    .catch(err => wrapper.error('fail', 'failed insert data', err.statusCode));
};

const deleteRecord = async (payload) => {
  let connections = await getConnection.pool.acquire();
  let result = new Promise((resolve, reject) => {
    connections.delete({
      index: payload.index,
      id: payload.id,
      type: payload.type
    }, async (error, response, status) => {
      getConnection.pool.release(connections);
      if (error) {
        reject(error);
      }
      resolve({
        response: response,
        status: status
      });
    });
  });
  return Promise.resolve(result)
    .then(ress => wrapper.data(ress.response, 'Successfully delete data', ress.status))
    .catch(err => wrapper.error('fail', 'failed to delet data', err.statusCode));
};

const search = async (payload) => {
  let connections = await getConnection.pool.acquire();
  let result = new Promise((resolve, reject) => {
    connections.search({
      index: payload.index,
      type: payload.type,
      body: {
        query: payload.data
      }
    }, async (error, response, status) => {
      getConnection.pool.release(connections);
      if (error) {
        reject(error);
      }
      resolve({
        response: response,
        status: status
      });
    });
  });
  return await Promise.resolve(result)
    .then(ress => wrapper.data(ress.response, 'success get data', ress.status))
    .catch(err => wrapper.error('fail', 'failed get data', err.statusCode));
};

const searchAll = async (payload) => {
  let connections = await getConnection.pool.acquire();
  let result = new Promise((resolve, reject) => {
    connections.search({
      index: payload.index,
      type: payload.type,
      body: payload.data
    }, async (error, response, status) => {
      getConnection.pool.release(connections);
      if (error) {
        reject(error);
      }
      resolve({
        response: response,
        status: status
      });
    });
  });
  return await Promise.resolve(result)
    .then(ress => wrapper.data(ress.response, 'success get data', ress.status))
    .catch(err => wrapper.error('fail', 'failed get data', err.statusCode));
};
module.exports = {
  insertOne,
  deleteRecord,
  search,
  searchAll
};
