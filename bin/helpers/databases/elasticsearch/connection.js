const es = require('elasticsearch');
const genericPool = require('generic-pool');
const config = require('../../../config');

const factory = {
  create: function () {
    const client = new es.Client(config.get('/elasticSearchConfig'));
    return client;
  },
  destroy: function (client) {
    client.close();
  }
};
const pool = genericPool.createPool(factory, config.get('/elasticSearchPool'));

module.exports = {
  pool
};
