/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 1/18/2020
 */
const {BadRequestError, InternalServerError} = require('../helpers/error');
const logger = require('../helpers/utils/logger');
const AuthenticationService = require('./clientAuthorizationService');
const authenticationService = new AuthenticationService();
const config = require('../config');
const getStreetListEndpoint = config.get('/getStreetList');
const rp = require('request-promise');
const _ = require('lodash');

/**
 * Get street list
 * @param {String} legacyCodeCity
 * @param {String} legacyCodeDistrict
 * @returns {Promise<*>}
 */
module.exports.getStreetList = async (legacyCodeCity, legacyCodeDistrict) => {
    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;

    if (_.isEmpty(legacyCodeCity) || _.isEmpty(legacyCodeDistrict)) {
        throw new BadRequestError('legacyCodeCity or legacyCodeDistrict cannot be empty');
    }

    const reqBody = {
        legacyCodeCity: legacyCodeCity,
        legacyCodeDistrict : legacyCodeDistrict
    };

    const options = {
        method: 'POST',
        uri: getStreetListEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorizationHeader
        },
        json: reqBody,
        strictSSL: false
    };
    logger.log('MetadataService','Sending request to get street list',`Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);

    if (response.statusCode === '0') {
        logger.log('MetadataService','Received street list', 'success');
        return response.data.streets;
    } else if(response.statusCode === '-2' && response.returnMessage === 'Data Not Found') {
        logger.log('MetadataService','Received empty street list', 'success');
        return [];
    }

    logger.log('MetadataService','ERROR in get street list.',`Req: ${JSON.stringify(response)}`);
    throw new InternalServerError(`ERROR in get street list ${JSON.stringify(response)}`);
};
