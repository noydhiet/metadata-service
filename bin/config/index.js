require('dotenv').config();
const confidence = require('confidence');

const config = {
  port: process.env.PORT,
  basicAuthApi: [
    {
      username: process.env.BASIC_AUTH_USERNAME,
      password: process.env.BASIC_AUTH_PASSWORD
    }
  ],
  authorization: {
    host: process.env.AUTHORIZATION_API_HOST,
    endpoint: process.env.AUTHORIZATION_API_ENDPOINT,
    token: process.env.AUTHORIZATION_TOKEN,
    timeOut: process.env.AUTHORIZATION_TIMEOUT
  },
  authenticationBaseUrl: process.env.AUTHENTICATION_BASE_URL,
  indihomeBaseUrl: process.env.INDIHOME_BASE_URL,
  indihomeSenderId: process.env.INDIHOME_SENDER_ID,
  indihomeUsername: process.env.INDIHOME_USERNAME,
  indihomePassword: process.env.INDIHOME_PASSWORD,
  jwtAuthKey: process.env.JWT_AUTH_KEY,
  emailUsername: process.env.EMAIL_USERNAME,
  emailPassword: process.env.EMAIL_PASSWORD,
  dsnSentryUrl: process.env.DSN_SENTRY_URL,
  firebase: {
    type: process.env.FIREBASE_TYPE,
    project_id: process.env.FIREBASE_PROJECT_ID,
    private_key_id: process.env.FIREBASE_PRIVATE_KEY_ID,
    private_key: process.env.FIREBASE_PRIVATE_KEY,
    client_email: process.env.FIREBASE_CLIENT_EMAIL,
    client_id: process.env.FIREBASE_CLIENT_ID,
    auth_uri: process.env.FIREBASE_AUTH_URI,
    token_uri: process.env.FIREBASE_TOKEN_URI,
    auth_provider_x509_cert_url: process.env.FIREBASE_AUTH_PROVIDER_X509_CERT_URL,
    client_x509_cert_url: process.env.FIREBASE_CLIENT_X509_CERT_URL
  },
  elasticSearchConfig: {
    host: process.env.ELASTIC_SEARCH_URL,
    connectionClass: process.env.ELASTIC_SEARCH_CONNECTION_CLASS,
    apiVersion: process.env.ELASTIC_SEARCH_API_VERSION,
    log: process.env.ELASTIC_SEARCH_LOG
  },
  elasticSearchPool: {
    max: parseInt(process.env.ELASTIC_SEARCH_MAX),
    min: parseInt(process.env.ELASTIC_SEARCH_MIN),
    idleTimeoutMillis: parseInt(process.env.ELASTIC_SEARCH_IDLE)
  },
  telkomUrl: process.env.TELKOM_API_URL,
  getStreetList: process.env.GET_STREET_LIST_ENDPOINT
};

const store = new confidence.Store(config);

exports.get = (key) => store.get(key);
