/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 1/18/2020
 */
const metadataService = require('../services/metadataService');
const wrapper = require('../helpers/utils/wrapper');
const logger = require('../helpers/utils/logger');
const _ = require('lodash');

/**
* Get street list
*/
module.exports.getStreetList = (req, res) => {
    const legacyCodeCity = req.query.legacyCodeCity;
    const legacyCodeDistrict = req.query.legacyCodeDistrict;
    metadataService.getStreetList(legacyCodeCity, legacyCodeDistrict)
        .then(resp => {
            if(_.isEmpty(resp)) {
                logger.log('MetadataController','success',`Received empty street list. Response: ${JSON.stringify(resp)}`);
                return wrapper.response(res, 'success', wrapper.data(resp), 'Retrieved empty street list', 200);
            }
            logger.log('MetadataController','success',`Received street list successfully. Response: ${JSON.stringify(resp)}`);
            wrapper.response(res, 'success', wrapper.data(resp), 'Retrieved street list successfully', 200);
        })
        .catch(err => {
            logger.log('MetadataController','fail',`Error in get street list. Error: ${JSON.stringify(err)}`);
            wrapper.response(res, 'fail', wrapper.error(err));
        });
};
