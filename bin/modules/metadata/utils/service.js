const rp = require('request-promise');
const config = require('../../../config');
const elastic = require('../../../helpers/databases/elasticsearch/elasticsearch');
const logger = require('../../../helpers/utils/logger');
const AuthenticationService = require('../../../services/clientAuthorizationService');
const authenticationService= new AuthenticationService();
const wrapper = require('../../../helpers/utils/wrapper');
const {ConflictError} = require('../../../helpers/error');

class Services {

  async getDistrict(data) {

    const authorizationHeader = `Bearer ${await authenticationService.getJWTFForUser()}`;
    let options = {
      uri: `${config.get('/telkomUrl')}/telkom-starclick-districtlist/1.0/getDistrictList`,
      headers: {
        'Authorization': authorizationHeader,
        'Content-Type': 'application/json'
      },
      body: {
        legacyCodeCity: data.legacyCodeCity
      },
      json: true,
      strictSSL: false
    };
    try {

      console.log(`request: ${JSON.stringify(options)}`);
      const result = await rp.post(options);
      logger.log('getDistrict','info',JSON.stringify(result));
      return result;
    } catch (error) {
      logger.log('getDistrict','error',error);
      return wrapper.error('fail', new ConflictError(error.message));
    }
  }

  async handleDistrictExpired(data) {
    const options = {
      uri: `${config.get('/telkomUrl')}/telkom-starclick-districtlist/1.0/getDistrictList`,
      headers: {
        'Authorization': `Bearer ${data.token}`,
        'Content-Type': 'application/json'
      },
      body: {
        legacyCodeCity: data.legacyCodeCity
      },
      json: true,
      strictSSL: false
    };
    try {
      const result = await rp.post(options);
      return result;
    } catch (error) {
      return wrapper.error('fail', 'Internal server error', 500);
    }
  }

  async getStreeList(data) {
    let options = {
      uri: `${config.get('/telkomUrl')}/telkom-starclick-streetlist/1.0/getStreetList`,
      headers: {
        'Authorization': `Bearer ${await authenticationService.getJWTFForUser()}`,
        'Content-Type': 'application/json'
      },
      body: {
        legacyCodeCity: data.legacyCodeCity,
        legacyCodeDistrict: data.legacyCodeDistrict
      },
      json: true,
      strictSSL: false
    };
    try {
      const result = await rp.post(options);
      logger.log('getStreeList','info',JSON.stringify(result));
      return result;
    } catch (error) {
      logger.log('getStreeList','info',error);
      return wrapper.error('fail', new ConflictError(error.message));
    }
  }

  async handleStreetExpired(data) {
    const options = {
      uri: `${config.get('/telkomUrl')}/telkom-starclick-streetlist/1.0/getStreetList`,
      headers: {
        'Authorization': `Bearer ${data.token}`,
        'Content-Type': 'application/json'
      },
      body: {
        legacyCodeCity: data.legacyCodeCity,
        legacyCodeDistrict: data.legacyCodeDistrict
      },
      json: true,
      strictSSL: false
    };
    try {
      const result = await rp.post(options);
      return result;
    } catch (error) {
      return wrapper.error('fail', 'Internal server error', 500);
    }
  }

  async getJwtToken() {
    const options = {
      uri: process.env.TELKOM_JWT_URL,
      headers: {
        'Authorization': `Basic dXNlcmRzczpEc3MjMTIz`,
        'Content-Type': 'application/json'
      },
      strictSSL: false
    };
    try {
      logger.log('JWT',`Jwt Req: ${JSON.stringify(options)}`);
      const result = await rp.post(options);
      logger.log('JWT',`Jwt Res: ${JSON.stringify(result)}`);
      return JSON.parse(result);
    } catch (error) {
      return wrapper.error('fail', 'Fail get data device Id', 500);
    }
  }

  async _getJwtToken() {
    let options = {
      method: 'GET',
      url: process.env.AUTHENTICATION_BASE_URL + process.env.AUTHORIZATION_API_ENDPOINT,
      headers:
          {
            'Authorization': `Basic ${process.env.AUTHORIZATION_TOKEN}`,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
      json: true,
      strictSSL: false
    };

    return await rp(options);
  }
}

module.exports = Services;
