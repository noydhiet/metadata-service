const wrapper = require('../../../helpers/utils/wrapper');
const Metadata = require('../repositories/metadata_repositories');
const model = require('../domains/metadata');
const validator = require('../../../helpers/utils/validator');

const getProvinces = async (req, res) => {
  const payload = req.query;
  const validatePayload = validator.isValidPayload(payload, model.province);
  const metadata = new Metadata();
  const postData = async (result) => {
    if(result.err) {
      return result;
    }
    return metadata.getProvinces(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Cant get provinces')
      : wrapper.response(res, 'success', result, 'Success get provinces');
  };
  sendResponse(await postData(await validatePayload));
};

const getCities = async (req, res) => {
  const payload = req.query;
  const validatePayload = validator.isValidPayload(payload, model.city);
  const metadata = new Metadata();
  const postData = async (result) => {
    if(result.err) {
      return result;
    }
    return metadata.getCities(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Cant get cities')
      : wrapper.response(res, 'success', result, 'Success get cities');
  };
  sendResponse(await postData(await validatePayload));
};

const getDistricts = async (req, res) => {
  const payload = req.query;
  const validatePayload = validator.isValidPayload(payload, model.district);
  const metadata = new Metadata();
  const postData = async (result) => {
    if(result.err) {
      return result;
    }
    return metadata.getDistricts(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Cant get district')
      : wrapper.response(res, 'success', result, 'Success get district');
  };
  sendResponse(await postData(await validatePayload));
};

const getSubDistricts = async (req, res) => {
  const payload = req.query;
  const validatePayload = validator.isValidPayload(payload, model.subdistrict);
  const metadata = new Metadata();
  const postData = async (result) => {
    if(result.err) {
      return result;
    }
    return metadata.getSubDistricts(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Cant get subdistricts')
      : wrapper.response(res, 'success', result, 'Success get subdistricts');
  };
  sendResponse(await postData(await validatePayload));
};

const getDistrictCore = async (req, res) => {
  const payload = req.query;
  const validatePayload = validator.isValidPayload(payload, model.districtCore);
  const metadata = new Metadata();
  const postData = async (result) => {
    if(result.err) {
      return result;
    }
    return metadata.getDistrictCore(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Cant get district')
      : wrapper.response(res, 'success', result, 'Success get district');
  };
  sendResponse(await postData(await validatePayload));
};

const getStreeList = async (req, res) => {
  const payload = req.query;
  const validatePayload = validator.isValidPayload(payload, model.streetCore);
  const metadata = new Metadata();
  const postData = async (result) => {
    if(result.err) {
      return result;
    }
    return metadata.getStreeList(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Cant get street')
      : wrapper.response(res, 'success', result, 'Success get street');
  };
  sendResponse(await postData(await validatePayload));
};

module.exports = {
  getProvinces,
  getCities,
  getDistricts,
  getSubDistricts,
  getDistrictCore,
  getStreeList
};
