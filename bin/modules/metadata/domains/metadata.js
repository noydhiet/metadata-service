const joi = require('joi');

const province = joi.object({
  search: joi.string().optional().allow('')
});

const city = joi.object({
  provinceId: joi.string().required(),
  search: joi.string().optional().allow('')
});

const district = joi.object({
  cityId: joi.string().required(),
  search: joi.string().optional().allow('')
});

const subdistrict = joi.object({
  districtId: joi.string().required(),
  search: joi.string().optional().allow('')
});

const districtCore = joi.object({
  legacyCodeCity: joi.string().required()
});

const streetCore = joi.object({
  legacyCodeCity: joi.string().required(),
  legacyCodeDistrict: joi.string().required()
});

const dataProvince = () => {
  const model = {
    id: '',
    islandId: '',
    name: '',
    created: '',
    lastModified: '',
    version: ''
  };
  return model;
};

module.exports = {
  province,
  city,
  district,
  subdistrict,
  dataProvince,
  districtCore,
  streetCore
};
