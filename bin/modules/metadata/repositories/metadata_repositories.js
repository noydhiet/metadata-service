const validate = require('validate.js');
const elastic = require('../../../helpers/databases/elasticsearch/elasticsearch');
const wrapper = require('../../../helpers/utils/wrapper');
const logger = require('../../../helpers/utils/logger');
const { InternalServerError, NotFoundError } = require('../../../helpers/error');
const Services = require('../utils/service');
const services = new Services();

class Metadata {
  async getProvinces(data) {
    let payload = {};
    if (validate.isEmpty(data.search)) {
      payload = {
        index: 'myindihome-data-province',
        type: '_doc',
        data: {
          size: 10000,
          sort: {'name.keyword': 'asc' },
          query: {
            match_all: {}
          }
        }
      };
    } else {
      payload = {
        index: 'myindihome-data-province',
        type: '_doc',
        data: {
          size: 10000,
          sort: {'name.keyword': 'asc' },
          query: {
            bool: {
              must: [
                {
                  query_string: {
                    fields: [
                      'name'
                    ],
                    query: `*${data.search}*`,
                    minimum_should_match: '100%'
                  }
                }
              ]
            }
          }
        }
      };
    }
    const result = await elastic.searchAll(payload);
    if (result.err) {
      logger.log('getAllProvince', 'fail',result.err );
      return wrapper.error(new InternalServerError('Internal server error'));
    }
    if (validate.isEmpty(result.data.hits.hits)) {
      logger.log('getAllProvince', 'fail', 'Province not found');
      return wrapper.error(new NotFoundError('Province Not Found'));
    }
    let arrProvince = [];
    result.data.hits.hits.map(value => {
      arrProvince.push(value._source);
    });
    return wrapper.data(arrProvince);
  }

  async getCities(data) {
    let payload = {};
    if (validate.isEmpty(data.search)) {
      payload = {
        index: 'myindihome-data-city',
        type: '_doc',
        data: {
          size: 10000,
          sort: { 'name.keyword': 'asc' },
          query: {
            bool: {
              must: [
                { match: { 'province.id': data.provinceId } },
                { match: { active: 'Y'}}
              ]
            }
          }
        }
      };
    } else {
      payload = {
        index: 'myindihome-data-city',
        type: '_doc',
        data: {
          size: 10000,
          sort: { 'name.keyword': 'asc' },
          query: {
            bool: {
              must: [
                { match: { 'province.id': data.provinceId } },
                { match: { active: 'Y'}},
                {
                  query_string: {
                    fields: [
                      'name'
                    ],
                    query: `*${data.search}*`,
                    minimum_should_match: '100%'
                  }
                }
              ]
            }
          }
        }
      };
    }
    const result = await elastic.searchAll(payload);
    if (result.err) {
      logger.log('getAllCity', 'fail', result.err);
      return wrapper.error(new InternalServerError('Internal server error'));
    }
    if (validate.isEmpty(result.data.hits.hits)) {
      logger.log('getAllCity', 'fail', 'Cities not found');
      return wrapper.data([]);
    }
    let arrCity = [];
    result.data.hits.hits.map(value => {
      arrCity.push(value._source);
    });
    return wrapper.data(arrCity);
  }

  async getDistricts(data) {
    let payload = {};
    if (validate.isEmpty(data.search)) {
      payload = {
        index: 'myindihome-data-district',
        type: '_doc',
        data: {
          size: 10000,
          sort: { 'name.keyword': 'asc' },
          query: {
            bool: {
              must: [
                { match: { 'city.id': data.cityId } },
                { match: { active: 'Y'}}
              ]
            }
          }
        }
      };
    } else {
      payload = {
        index: 'myindihome-data-district',
        type: '_doc',
        data: {
          size: 10000,
          sort: { 'name.keyword': 'asc' },
          query: {
            bool: {
              must: [
                { match: { 'city.id': data.cityId } },
                { match: { active: 'Y'}},
                {
                  query_string: {
                    fields: [
                      'name'
                    ],
                    query: `*${data.search}*`,
                    minimum_should_match: '100%'
                  }
                }
              ]
            }
          }
        }
      };
    }
    const result = await elastic.searchAll(payload);
    if (result.err) {
      logger.log('getAllDistricts', 'fail',result.err);
      return wrapper.error(new InternalServerError('Internal server error'));
    }
    if (validate.isEmpty(result.data.hits.hits)) {
      logger.log('getAllDistricts', 'fail', 'District not found');
      return wrapper.error(new NotFoundError('District Not Found'));
    }
    let arrDistrict = [];
    result.data.hits.hits.map(value => {
      arrDistrict.push(value._source);
    });
    return wrapper.data(arrDistrict);
  }

  async getSubDistricts(data) {
    let payload = {};
    if (validate.isEmpty(data.search)) {
      payload = {
        index: 'myindihome-data-subdistrict',
        type: '_doc',
        data: {
          sort: { name: 'asc' },
          size: 10000,
          query: {
            bool: {
              must: [
                { match: { 'district.id': data.districtId } }
              ]
            }
          }
        }
      };
    } else {
      payload = {
        index: 'myindihome-data-subdistrict',
        type: '_doc',
        data: {
          sort: { name: 'asc' },
          size: 10000,
          query: {
            bool: {
              must: [
                { match: { 'district.id': data.districtId } },
                {
                  query_string: {
                    fields: [
                      'name'
                    ],
                    query: `*${data.search}*`,
                    minimum_should_match: '100%'
                  }
                }
              ]
            }
          }
        }
      };
    }
    const result = await elastic.searchAll(payload);
    if (result.err) {
      logger.log('getAllSubDistrict', 'fail', result.err);
      return wrapper.error(new InternalServerError('Internal server error'));
    }
    if (validate.isEmpty(result.data.hits.hits)) {
      logger.log('getAllSubDistrict', 'fail', 'Subdistricts not found');
      return wrapper.error(new NotFoundError('Subdistricts Not Found'));
    }
    return wrapper.data(result.data.hits.hits);
  }

  async getDistrictCore(data){

    let requestPayload = {
      token: '',
      legacyCodeCity: data.legacyCodeCity
    };

    const result = await services.getDistrict(requestPayload);
    if(result.err || validate.isEmpty(result.data)){
      logger.log('getDistrictCore', 'fail', 'District not found');
      return wrapper.data([]);
    }
    logger.log('getDistrictCore', 'success', 'Get district success');
    return wrapper.data(result.data.districts);
  }

  async getStreeList(data){
    const payload = {
      index: 'myindihome-data-token',
      type: '_doc',
      data: {
        query: {
          match_all: {}
        }
      }
    };
    const getToken = await elastic.searchAll(payload);
    let requestPayload = {};
    if(getToken.err){
      requestPayload = {
        token: '',
        legacyCodeCity: data.legacyCodeCity,
        legacyCodeDistrict: data.legacyCodeDistrict
      };
    }else{
      requestPayload = {
        token: validate.isEmpty(getToken.data.hits.hits[0]._source.token) ? '' : getToken.data.hits.hits[0]._source.token,
        legacyCodeCity: data.legacyCodeCity,
        legacyCodeDistrict: data.legacyCodeDistrict
      };
    }

    const result = await services.getStreeList(requestPayload);
    if(result.err || validate.isEmpty(result.data)){
      logger.log('getStreeList', 'fail', 'Street not found');
      return wrapper.data([]);
    }
    logger.log('getStreeList', 'success', 'Get street success');
    return wrapper.data(result.data.streets);
  }
}

module.exports = Metadata;

