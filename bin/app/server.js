const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const basicAuth = require('../auth/basic_auth_helper');
const wrapper = require('../helpers/utils/wrapper');
const swaggerDocument = require('../swagger/swagger.json');
const metadataHandler = require('../modules/metadata/handlers/metadata_handler');
const metadataController = require('../controllers/metadataController');

function AppServer() {
  this.server = express();

  this.server.use(cors());
  this.server.use(bodyParser.urlencoded({ extended: false }));
  this.server.use(bodyParser.json());

  // required for basic auth
  this.server.use(basicAuth.init());

  this.server.get('/', (req, res) => {
    wrapper.response(res, 'success', wrapper.data('User service'), 'This service is running properly.');
  });

  this.server.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

  this.server.get('/metadata/provinces', basicAuth.isAuthenticated, metadataHandler.getProvinces);
  this.server.get('/metadata/cities', basicAuth.isAuthenticated, metadataHandler.getCities);
  this.server.get('/metadata/districts', basicAuth.isAuthenticated, metadataHandler.getDistricts);
  this.server.get('/metadata/subdistricts', basicAuth.isAuthenticated, metadataHandler.getSubDistricts);
  this.server.get('/metadata/districts-core', basicAuth.isAuthenticated, metadataHandler.getDistrictCore);
  this.server.get('/metadata/streets', basicAuth.isAuthenticated, metadataController.getStreetList);
}

module.exports = AppServer;
